function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  localStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  localStorage.setItem("json", JSON.stringify(objeto));
}
function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = localStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(localStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function sesionStorage(op){
  console.log(op);
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var spanTotal = document.getElementById("spanTotal");
  var clave = txtClave.value;
  var valor = txtValor.value;
  if(op == 'G'){
    sessionStorage.setItem(clave, valor);
  }else if(op == 'R'){
    var spanValor = document.getElementById("spanValor");
    spanValor.innerText = sessionStorage.getItem(clave);
  }else if(op == 'E'){
    sessionStorage.removeItem(clave);
  }else if(op == 'L'){
    sessionStorage.clear();
  }
  spanTotal.innerText = 'Total: ' + sessionStorage.length;
}